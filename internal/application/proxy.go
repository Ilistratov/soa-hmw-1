package application

import (
	"fmt"

	"gitlab.com/Ilistratov/soa-hmw-1/config"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/adapter"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/port"
)

type ProxyApp struct {
	AppBase
}

func NewProxyApp(cfg *config.Config, proxyCfg *config.ProxyConfig) (*ProxyApp, error) {
	res := ProxyApp{}
	sources := make(map[string]port.ResultSource)
	for format, addr := range proxyCfg.TesterAddr {
		sources[format] = adapter.NewHTTPResultReceiver(addr)
	}

	var proxy port.Service
	var err error
	if cfg.ComMode == "HTPP" {
		proxy, err = adapter.NewHTTPTesterProxy(cfg, sources)
	} else if cfg.ComMode == "UDP" {
		proxy, err = adapter.NewUDPTesterProxy(cfg, sources)
	} else {
		err = fmt.Errorf("unknown ComMode: %v", cfg.ComMode)
	}

	if err != nil {
		return nil, err
	}
	res.services = append(res.services, proxy)
	return &res, nil
}
