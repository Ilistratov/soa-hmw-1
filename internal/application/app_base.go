package application

import (
	"context"
	"log"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/port"
)

type AppBase struct {
	services []port.Service
}

func (a *AppBase) Start() error {
	for idx, s := range a.services {
		err := s.Start()
		if err == nil {
			continue
		}
		log.Printf("failed to start service: %v", err)
		a.services = a.services[0:idx]
		return err
	}
	return nil
}

func (a *AppBase) Stop(ctx context.Context) error {
	for _, s := range a.services {
		err := s.Stop(ctx)
		if err != nil {
			log.Printf("shutdown error: %v", err)
		}
	}
	return nil
}
