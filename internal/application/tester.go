package application

import (
	"fmt"

	"gitlab.com/Ilistratov/soa-hmw-1/config"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/adapter"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/port"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/runners"
)

type TesterApp struct {
	AppBase
}

func NewTesterApp(cfg *config.Config) (*TesterApp, error) {
	res := TesterApp{}

	var specificRunner port.TestRunner
	switch cfg.ServiceRole {
	case "JSON":
		specificRunner = runners.NewJsonTester()
	case "XML":
		specificRunner = runners.NewXMLTester()
	case "YAML":
		specificRunner = runners.NewYAMLTester()
	case "MSGPack":
		specificRunner = runners.NewMSGPackTester()
	case "AVRO":
		specificRunner = runners.NewAvroTester()
	case "Protobuf":
		specificRunner = runners.NewProtoTester()
	case "GOB":
		specificRunner = runners.NewGobTester()
	default:
		return nil, fmt.Errorf("unsupported format: %v", cfg.ServiceRole)
	}

	runner, err := adapter.NewGenericRunner(specificRunner)
	if err != nil {
		return nil, err
	}
	res.services = append(res.services, runner)

	http, err := adapter.NewResultSender(cfg, runner)
	if err != nil {
		return nil, err
	}
	res.services = append(res.services, http)
	return &res, nil
}
