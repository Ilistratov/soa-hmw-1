package runners

import (
	"encoding/xml"
	"log"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
)

type XMLTester struct {
	buffer []byte
	val    *entity.TestStruct
}

func NewXMLTester() *XMLTester {
	return &XMLTester{
		val: entity.NewTestStruct(),
	}
}

func (r *XMLTester) FormatName() string {
	return "XML"
}

func (r *XMLTester) Serialize() uint32 {
	res, err := xml.Marshal(r.val)
	if err != nil {
		log.Fatalf("serialization failed: %v", err)
	}
	r.buffer = res
	return uint32(len(res))
}

func (r *XMLTester) Deserialize() {
	t := entity.TestStruct{}
	err := xml.Unmarshal(r.buffer, &t)
	if err != nil {
		log.Fatalf("deserialization failed: %v", err)
	}
}
