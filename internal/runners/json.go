package runners

import (
	"encoding/json"
	"log"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
)

type JsonTester struct {
	buffer []byte
	val    *entity.TestStruct
}

func NewJsonTester() *JsonTester {
	return &JsonTester{
		val: entity.NewTestStruct(),
	}
}

func (r *JsonTester) FormatName() string {
	return "JSON"
}

func (r *JsonTester) Serialize() uint32 {
	res, err := json.Marshal(r.val)
	if err != nil {
		log.Fatalf("serialization failed: %v", err)
	}
	r.buffer = res
	return uint32(len(res))
}

func (r *JsonTester) Deserialize() {
	t := entity.TestStruct{}
	err := json.Unmarshal(r.buffer, &t)
	if err != nil {
		log.Fatalf("deserialization failed: %v", err)
	}
}
