package runners

import (
	"log"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
	"gopkg.in/yaml.v2"
)

type YAMLTester struct {
	buffer []byte
	val    *entity.TestStruct
}

func NewYAMLTester() *YAMLTester {
	return &YAMLTester{
		val: entity.NewTestStruct(),
	}
}

func (r *YAMLTester) FormatName() string {
	return "YAML"
}

func (r *YAMLTester) Serialize() uint32 {
	res, err := yaml.Marshal(r.val)
	if err != nil {
		log.Fatalf("serialization failed: %v", err)
	}
	r.buffer = res
	return uint32(len(res))
}

func (r *YAMLTester) Deserialize() {
	t := entity.TestStruct{}
	err := yaml.Unmarshal(r.buffer, &t)
	if err != nil {
		log.Fatalf("deserialization failed: %v", err)
	}
}
