package runners

import (
	"log"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
	protomsg "gitlab.com/Ilistratov/soa-hmw-1/internal/proto"
	"google.golang.org/protobuf/proto"
)

type ProtobufTester struct {
	buffer []byte
	msg    protomsg.TestStruct
}

func NewProtoTester() *ProtobufTester {
	res := ProtobufTester{}
	test_struct := entity.NewTestStruct()
	res.msg.TestStr = test_struct.TestStr
	res.msg.TestFloat = test_struct.TestFloat
	res.msg.TestFloatLong = test_struct.TestFloatLong
	res.msg.TestInt = test_struct.TestInt
	res.msg.TestIntLong = test_struct.TestIntLong
	res.msg.TestArr = test_struct.TestArr
	res.msg.Mapping = test_struct.Mapping
	return &res
}

func (r *ProtobufTester) FormatName() string {
	return "Protobuf"
}

func (r *ProtobufTester) Serialize() uint32 {
	res, err := proto.Marshal(&r.msg)
	if err != nil {
		log.Fatalf("serialization failed: %v", err)
	}
	r.buffer = res
	return uint32(len(res))
}

func (r *ProtobufTester) Deserialize() {
	err := proto.Unmarshal(r.buffer, &r.msg)
	if err != nil {
		log.Fatalf("deserialization failed: %v", err)
	}
}
