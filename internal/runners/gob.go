package runners

import (
	"bytes"
	"encoding/gob"
	"log"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
)

type GobTester struct {
	buffer  bytes.Buffer
	val     *entity.TestStruct
	encoder gob.Encoder
	decoder gob.Decoder
}

func NewGobTester() *GobTester {
	res := GobTester{
		buffer: bytes.Buffer{},
		val:    entity.NewTestStruct(),
	}
	gob.Register(*res.val)
	res.encoder = *gob.NewEncoder(&res.buffer)
	res.decoder = *gob.NewDecoder(&res.buffer)
	return &res
}

func (r *GobTester) FormatName() string {
	return "GOB"
}

func (r *GobTester) Serialize() uint32 {
	err := r.encoder.Encode(r.val)
	if err != nil {
		log.Fatalf("serialization failed: %v", err)
	}
	return uint32(r.buffer.Len())
}

func (r *GobTester) Deserialize() {
	t := entity.TestStruct{}
	err := r.decoder.Decode(&t)
	r.buffer.Reset()
	if err != nil {
		log.Fatalf("deserialization failed: %v", err)
	}
}
