package runners

import (
	"log"

	"github.com/hamba/avro"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
)

type AvroTester struct {
	schema avro.Schema
	buffer []byte
	val    *entity.TestStruct
}

func NewAvroTester() *AvroTester {
	res := AvroTester{
		schema: avro.MustParse(entity.ArvoShema),
		val:    entity.NewTestStruct(),
	}
	return &res
}

func (r *AvroTester) FormatName() string {
	return "AVRO"
}

func (r *AvroTester) Serialize() uint32 {
	res, err := avro.Marshal(r.schema, r.val)
	if err != nil {
		log.Fatalf("serialization failed: %v", err)
	}
	r.buffer = res
	return uint32(len(res))
}

func (r *AvroTester) Deserialize() {
	t := entity.TestStruct{}
	err := avro.Unmarshal(r.schema, r.buffer, &t)
	if err != nil {
		log.Fatalf("deserialization failed: %v", err)
	}
}
