package runners

import (
	"log"

	"github.com/vmihailenco/msgpack"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
)

type MSGPackTester struct {
	buffer []byte
	val    *entity.TestStruct
}

func NewMSGPackTester() *MSGPackTester {
	return &MSGPackTester{
		val: entity.NewTestStruct(),
	}
}

func (r *MSGPackTester) FormatName() string {
	return "MSGPack"
}

func (r *MSGPackTester) Serialize() uint32 {
	res, err := msgpack.Marshal(r.val)
	if err != nil {
		log.Fatalf("serialization failed: %v", err)
	}
	r.buffer = res
	return uint32(len(res))
}

func (r *MSGPackTester) Deserialize() {
	t := entity.TestStruct{}
	err := msgpack.Unmarshal(r.buffer, &t)
	if err != nil {
		log.Fatalf("deserialization failed: %v", err)
	}
}
