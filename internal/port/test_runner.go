package port

type TestRunner interface {
	FormatName() string
	Serialize() uint32
	Deserialize()
}
