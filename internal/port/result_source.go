package port

import (
	"context"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
)

type ResultSource interface {
	GetResult(context.Context) (entity.TestResult, error)
}
