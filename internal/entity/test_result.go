package entity

import (
	"fmt"
	"strings"
	"time"
)

type TestResult struct {
	FormatName          string
	StructSize          uint32
	SerializationTime   time.Duration
	DeserializationTime time.Duration
}

const testResultFormat = "%s-%d-%dms-%dms"

func (t *TestResult) ToString() string {
	return fmt.Sprintf(testResultFormat, t.FormatName, t.StructSize, t.SerializationTime.Milliseconds(), t.DeserializationTime.Milliseconds())
}

func (t *TestResult) FromString(s string) error {
	s = strings.Replace(s, "-", " ", -1)
	scanFormat := strings.Replace(testResultFormat, "-", " ", -1)
	var serializeTimeMS, deserializeTimeMS uint32
	_, err := fmt.Sscanf(s, scanFormat, &t.FormatName, &t.StructSize, &serializeTimeMS, &deserializeTimeMS)
	t.SerializationTime = time.Duration(serializeTimeMS * uint32(time.Millisecond))
	t.DeserializationTime = time.Duration(deserializeTimeMS * uint32(time.Millisecond))
	return err
}
