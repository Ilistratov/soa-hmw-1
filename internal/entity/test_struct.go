package entity

import "fmt"

type TestStruct struct {
	TestStr       string
	TestFloat     float32
	TestFloatLong float64
	TestInt       int32
	TestIntLong   int64
	TestArr       []int32
	Mapping       map[string]string `xml:"-"`
}

const ArvoShema = `
{
  "type": "record",
  "name": "TestStruct",
  "fields": [
    { "name": "TestStr", "type": "string" },
    { "name": "TestFloat", "type": "float" },
    { "name": "TestFloatLong", "type": "double" },
    { "name": "TestInt", "type": "int" },
    { "name": "TestIntLong", "type": "long" },
    { "name": "TestArr", "type": { "type": "array", "items": "int" } },
    { "name": "Mapping", "type": { "type": "map", "values": "string" } }
  ]
}
`

func NewTestStruct() *TestStruct {
	res := TestStruct{
		TestStr:       "",
		TestFloat:     3.14,
		TestFloatLong: 3.14159265359,
		TestInt:       42,
		TestIntLong:   int64(1<<30) * int64(1<<30),
		TestArr:       []int32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 12, 13},
		Mapping: map[string]string{
			"ABOBA":   "OBABO",
			"ABACABA": "BACACAB",
		},
	}
	for i := 1; i <= 2000; i++ {
		res.TestArr = append(res.TestArr, int32(i+(1<<30)))
	}
	res.TestStr = fmt.Sprintf("%v", res.TestArr)
	return &res
}
