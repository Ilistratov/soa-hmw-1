package adapter

import (
	"context"
	"fmt"
	"net"
	"strings"
	"time"

	"gitlab.com/Ilistratov/soa-hmw-1/config"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/port"
)

type UDPTesterProxy struct {
	server        net.UDPConn
	resultSources map[string]port.ResultSource
	done          chan struct{}
}

func (s *UDPTesterProxy) handleRequest(format string) string {
	testerRequestCtx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	if source, exists := s.resultSources[format]; exists {
		res, err := source.GetResult(testerRequestCtx)
		if err != nil {
			return err.Error() + "\n"
		}
		return res.ToString() + "\n"
	}
	if format != "all" {
		return fmt.Sprintf("unknown format: %v", format)
	}
	builder := strings.Builder{}
	for name, source := range s.resultSources {
		res, err := source.GetResult(testerRequestCtx)
		if err != nil {
			builder.WriteString(fmt.Sprintf("error for %v: %v\n", name, err))
		} else {
			builder.WriteString(res.ToString())
			builder.WriteString("\n")
		}
	}
	return builder.String()
}

func (s *UDPTesterProxy) listenAndServeLoop() {
	buf := make([]byte, 1024)
	for {
		msgLen, conn, err := s.server.ReadFromUDP(buf)
		if err != nil {
			break
		}
		format := string(buf[0:msgLen])
		format = strings.NewReplacer(" ", "", "\n", "").Replace(format)
		response := s.handleRequest(format)
		s.server.WriteToUDP([]byte(response), conn)
	}
	close(s.done)
}

func NewUDPTesterProxy(cfg *config.Config, resultSources map[string]port.ResultSource) (*UDPTesterProxy, error) {
	addr, err := net.ResolveUDPAddr("udp", fmt.Sprintf(":%v", cfg.Port))
	if err != nil {
		return nil, err
	}
	server, err := net.ListenUDP("udp", addr)
	if err != nil {
		return nil, err
	}
	return &UDPTesterProxy{
		server:        *server,
		resultSources: resultSources,
		done:          make(chan struct{}),
	}, nil
}

func (s *UDPTesterProxy) Start() error {
	go s.listenAndServeLoop()
	return nil
}

func (s *UDPTesterProxy) Stop(ctx context.Context) error {
	s.server.Close()
	<-s.done
	return nil
}
