package adapter

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
)

type HTTPResultReceiver struct {
	testSenderAddr string
}

func NewHTTPResultReceiver(addr string) *HTTPResultReceiver {
	return &HTTPResultReceiver{
		testSenderAddr: addr,
	}
}

func (r *HTTPResultReceiver) GetResult(ctx context.Context) (entity.TestResult, error) {
	request, err := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("http://%v/get_result", r.testSenderAddr), nil)
	if err != nil {
		return entity.TestResult{}, err
	}
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return entity.TestResult{}, err
	}
	if response.StatusCode != http.StatusOK {
		return entity.TestResult{}, errors.New(http.StatusText((response.StatusCode)))
	}
	buf := &bytes.Buffer{}
	_, err = buf.ReadFrom(response.Body)
	if err != nil {
		return entity.TestResult{}, err
	}
	result := entity.TestResult{}
	err = result.FromString(buf.String())
	if err != nil {
		return entity.TestResult{}, err
	}
	return result, nil
}
