package adapter

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/Ilistratov/soa-hmw-1/internal/entity"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/port"
)

type GenericTestRunner struct {
	specificRunner port.TestRunner
	done           chan struct{}
	result         entity.TestResult
}

func NewGenericRunner(specificRunner port.TestRunner) (*GenericTestRunner, error) {
	res := GenericTestRunner{
		specificRunner: specificRunner,
	}
	return &res, nil
}

const iterCount = 1000

func (r *GenericTestRunner) doTestRuns() {
	log.Printf("runs started")
	res := entity.TestResult{
		FormatName: r.specificRunner.FormatName(),
	}
	for i := 0; i < iterCount; i += 1 {
		serializationStart := time.Now()
		res.StructSize = r.specificRunner.Serialize() / 1024
		res.SerializationTime += time.Since(serializationStart)
		deserializationStart := time.Now()
		r.specificRunner.Deserialize()
		res.DeserializationTime += time.Since(deserializationStart)
	}
	log.Printf("runs done")
	r.result = res
	close(r.done)
}

func (r *GenericTestRunner) GetResult(ctx context.Context) (entity.TestResult, error) {
	select {
	case <-r.done:
		return r.result, nil
	case <-ctx.Done():
		return entity.TestResult{}, fmt.Errorf("get result timeout")
	}
}

func (r *GenericTestRunner) Start() error {
	r.done = make(chan struct{})
	go r.doTestRuns()
	return nil
}

func (r *GenericTestRunner) Stop(ctx context.Context) error {
	log.Printf("stopping")
	_, err := r.GetResult(ctx)
	return err
}
