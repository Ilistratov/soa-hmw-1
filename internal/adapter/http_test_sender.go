package adapter

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/Ilistratov/soa-hmw-1/config"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/port"
)

type HTTPResultSender struct {
	source port.ResultSource
	router *chi.Mux
	server http.Server
}

func (a *HTTPResultSender) OnGetResult(w http.ResponseWriter, r *http.Request) {
	testRes, err := a.source.GetResult(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	} else {
		w.Write([]byte(testRes.ToString()))
	}
}

func (a *HTTPResultSender) Start() error {
	go a.server.ListenAndServe()
	return nil
}

func (a *HTTPResultSender) Stop(ctx context.Context) error {
	return a.server.Shutdown(ctx)
}

func NewResultSender(cfg *config.Config, source port.ResultSource) (*HTTPResultSender, error) {
	res := HTTPResultSender{}
	res.source = source
	res.router = chi.NewRouter()
	res.router.Get("/get_result", res.OnGetResult)
	res.server = http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: res.router,
	}

	return &res, nil
}
