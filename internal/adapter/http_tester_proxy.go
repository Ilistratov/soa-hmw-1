package adapter

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/Ilistratov/soa-hmw-1/config"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/port"
)

type HTTPTesterProxy struct {
	router        *chi.Mux
	server        http.Server
	resultSources map[string]port.ResultSource
}

func (a *HTTPTesterProxy) onGetResult(w http.ResponseWriter, r *http.Request) {
	format := chi.URLParam(r, "format")
	if format == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("format not specified\n"))
		return
	}
	if source, exists := a.resultSources[format]; exists {
		res, err := source.GetResult(r.Context())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		} else {
			w.Write([]byte(res.ToString() + "\n"))
		}
		return
	}
	if format != "all" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("unsupported format: %v", format)))
		return
	}
	for format, source := range a.resultSources {
		res, err := source.GetResult(r.Context())
		if err != nil {
			w.Write([]byte(fmt.Sprintf("request for format %v failed: %v\n", format, err)))
		} else {
			w.Write([]byte(res.ToString() + "\n"))
		}
	}
}

func (a *HTTPTesterProxy) Start() error {
	go a.server.ListenAndServe()
	return nil
}

func (a *HTTPTesterProxy) Stop(ctx context.Context) error {
	return a.server.Shutdown(ctx)
}

func NewHTTPTesterProxy(cfg *config.Config, resultSources map[string]port.ResultSource) (*HTTPTesterProxy, error) {
	res := HTTPTesterProxy{}
	res.resultSources = resultSources
	res.router = chi.NewRouter()
	res.router.Get("/get_result/{format}", res.onGetResult)
	res.server = http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: res.router,
	}

	return &res, nil
}
