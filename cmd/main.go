package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/Ilistratov/soa-hmw-1/config"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/application"
	"gitlab.com/Ilistratov/soa-hmw-1/internal/port"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGTERM, os.Interrupt)
	defer cancel()

	cfg, err := config.LoadConfig()
	if err != nil {
		log.Fatalf("failed to load config: %v", err.Error())
	}

	var a port.Service
	if cfg.ServiceRole != "PROXY" {
		a, err = application.NewTesterApp(cfg)
		if err != nil {
			log.Fatalf("failed to initialize app: %v", err.Error())
		}
	} else {
		proxyCfg, err := config.LoadProxyConfig(cfg.ProxyConfigPath)
		if err != nil {
			log.Fatalf("failed to load proxy config: %v", err.Error())
		}
		a, err = application.NewProxyApp(cfg, proxyCfg)
		if err != nil {
			log.Fatalf("failed to initialize app: %v", err.Error())
		}
	}

	err = a.Start()
	if err != nil {
		log.Fatalf("failed to start app: %v", err.Error())
		stopCtx, cancelTimeout := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelTimeout()
		a.Stop(stopCtx)
	}

	<-ctx.Done()
	stopCtx, cancelTimeout := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelTimeout()

	err = a.Stop(stopCtx)
	if err != nil {
		log.Printf("error during server stop: %v", err.Error())
	}

}
