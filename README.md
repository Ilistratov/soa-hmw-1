##
Запуск: 
```
> docker compose up
```

## Config

Для переключения режимов коммуникации proxy используется переменная окружения `COM_MODE`. Если `COM_MODE` равен `UDP`, используется решим, описанный в `UDP messages`. Если `HTTP`, то режим из `REST API`

## UDP messages
proxy запускается и слушает UDP пакеты на порте 2000. Пакет должен содержать одну из следующих строчек:

- `all` запросить результаты для всех форматов
- `JSON`/`XML`/`YAML`/`MSGPack`/`AVRO`/`Protobuf`/`GOB` - запросить результаты для соотв. формата

Пример
```
> nc -u localhost 2000
> all
AVRO-31-212ms-354ms
Protobuf-31-170ms-274ms
GOB-31-161ms-358ms
JSON-43-712ms-2666ms
XML-78-2197ms-651ms
YAML-47-1740ms-2656ms
MSGPack-31-913ms-1134ms
> XML
XML-78-2202ms-1130ms
```

## REST API
proxy запускается и слушает HTTP запросы на порте 2000. Поддерживается запрос GET по url'у `/get_result/{TYPE}` где `TYPE` - 

- `all` запросить результаты для всех форматов
- `JSON`/`XML`/`YAML`/`MSGPack`/`AVRO`/`Protobuf`/`GOB` - запросить результаты для соотв. формата

Пример:
```
> curl -X GET http://localhost:2000/get_result/all

AVRO-31-212ms-354ms
Protobuf-31-170ms-274ms
GOB-31-161ms-358ms
JSON-43-712ms-2666ms
XML-78-2197ms-651ms
YAML-47-1740ms-2656ms
MSGPack-31-913ms-1134ms
```

```
> curl -X GET http://localhost:2000/get_result/XML 

XML-78-2202ms-1130ms
```