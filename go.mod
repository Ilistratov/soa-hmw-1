module gitlab.com/Ilistratov/soa-hmw-1

go 1.18

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/go-chi/chi/v5 v5.0.8
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/golang/protobuf v1.5.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)

require (
	github.com/hamba/avro v1.8.0
	github.com/stretchr/testify v1.8.2 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible
)
