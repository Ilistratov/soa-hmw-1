FROM golang:latest as builder

WORKDIR /app
COPY go.* ./
RUN go mod download
COPY . ./
WORKDIR /app/cmd
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -o application

FROM alpine:latest
WORKDIR /app
COPY --from=builder /app/cmd/application /app/application
CMD ["/app/application"]
