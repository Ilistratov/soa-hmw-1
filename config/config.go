package config

import (
	"os"

	"github.com/caarlos0/env"
	"gopkg.in/yaml.v2"
)

type Config struct {
	ServiceRole     string `env:"SERVICE_ROLE" envDefault:"JSON"`
	ProxyConfigPath string `env:"PROXY_CONFIG_PATH" envDefault:""`
	Port            string `env:"PORT" envDefault:"2000"`
	ComMode         string `env:"COM_MODE" envDefault:"HTTP"`
}

type ProxyConfig struct {
	TesterAddr map[string]string
}

func LoadConfig() (*Config, error) {
	cfg := Config{}
	err := env.Parse(&cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func LoadProxyConfig(path string) (*ProxyConfig, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	decoder := yaml.NewDecoder(file)
	res := ProxyConfig{}
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	return &res, nil
}
